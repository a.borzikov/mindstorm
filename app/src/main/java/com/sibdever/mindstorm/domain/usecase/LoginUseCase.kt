package com.sibdever.mindstorm.domain.usecase

import com.sibdever.mindstorm.data.repository.AuthRepository

class LoginUseCase(private val repo: AuthRepository) {

    suspend operator fun invoke(email: String, password: String) = repo.login(email, password)
}
