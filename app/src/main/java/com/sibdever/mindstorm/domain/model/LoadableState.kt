package com.sibdever.mindstorm.domain.model

sealed class LoadableState<out T> {

    object Initial : LoadableState<Nothing>()

    object Loading : LoadableState<Nothing>()

    class Success<T>(val data: T) : LoadableState<T>()

    class Error(val message: String?) : LoadableState<Nothing>() {

        constructor(throwable: Throwable) : this(throwable.message)
    }
}

fun <T> LoadableState<T>.isLoading() = this is LoadableState.Loading
fun <T> LoadableState<T>.isSuccess() = this is LoadableState.Success
fun <T> LoadableState<T>.forceData() = (this as? LoadableState.Success<T>)?.data
