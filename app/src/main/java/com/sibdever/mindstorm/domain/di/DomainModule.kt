package com.sibdever.mindstorm.domain.di

import com.sibdever.mindstorm.domain.usecase.LoginUseCase
import com.sibdever.mindstorm.domain.usecase.RegisterUseCase
import com.sibdever.mindstorm.helper.AuthCredentialsHelper
import com.sibdever.mindstorm.ui.di.UiModule.GLOBAL_SCOPE
import org.koin.core.qualifier.named
import org.koin.dsl.module

object DomainModule {

    val useCases = module {
        single { LoginUseCase(get()) }
        single { RegisterUseCase(get()) }
    }

    val helpers = module {
        single { AuthCredentialsHelper(get(named(GLOBAL_SCOPE)), get()) }
    }
}
