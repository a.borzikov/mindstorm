package com.sibdever.mindstorm.domain.usecase

import com.sibdever.mindstorm.data.repository.AuthRepository

class RegisterUseCase(private val repo: AuthRepository) {

    suspend operator fun invoke(email: String, username: String, password: String) =
        repo.register(email, username, password)
}
