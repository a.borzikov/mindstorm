package com.sibdever.mindstorm.domain.model

data class AuthCredentials(
    val accessToken: String,
    val registered: Boolean
)
