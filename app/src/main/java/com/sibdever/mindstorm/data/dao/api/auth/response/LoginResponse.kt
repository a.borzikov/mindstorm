package com.sibdever.mindstorm.data.dao.api.auth.response

data class LoginResponse(
    val token: String?
)
