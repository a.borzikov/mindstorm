package com.sibdever.mindstorm.data.dao.api.comon

import com.sibdever.mindstorm.data.dao.preferences.PreferencesDao
import okhttp3.Interceptor
import okhttp3.Response

class HeadersInterceptor(
    private val preferencesDao: PreferencesDao
) : Interceptor {

    companion object {

        const val HEADER_AUTHORIZATION = "Authorization"
    }

    override fun intercept(chain: Interceptor.Chain): Response {

        val requestBuilder = chain.request().newBuilder()

        preferencesDao.authCredentials
            ?.accessToken
            ?.takeIf { it.isNotBlank() }
            ?.let {
                requestBuilder.addHeader(HEADER_AUTHORIZATION, "Bearer $it")
            }

        return chain.proceed(requestBuilder.build())
    }
}
