package com.sibdever.mindstorm.data.dao.api.auth.response

data class RegisterResponse(
    val token: String?
)
