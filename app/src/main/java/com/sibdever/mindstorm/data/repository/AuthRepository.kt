package com.sibdever.mindstorm.data.repository

import com.sibdever.mindstorm.data.dao.api.auth.AuthApi
import com.sibdever.mindstorm.data.dao.api.auth.request.LoginRequest
import com.sibdever.mindstorm.data.dao.api.auth.request.RegisterRequest
import com.sibdever.mindstorm.data.dao.api.comon.NetworkUtil
import com.sibdever.mindstorm.data.dao.preferences.PreferencesDao
import com.sibdever.mindstorm.domain.model.AuthCredentials
import com.sibdever.mindstorm.exceptions.RequestException

class AuthRepository(
    private val networkUtil: NetworkUtil,
    private val authApi: AuthApi,
    private val preferencesDao: PreferencesDao
) {

    suspend fun register(email: String, username: String, password: String) {
        networkUtil.retry {
            authApi.register(RegisterRequest(email, username, password))
        }
        networkUtil.retry {
            authApi.login(LoginRequest(email, password))
        }.token?.let {
            preferencesDao.authCredentials = AuthCredentials(it, true)
        } ?: throw RequestException("Cannot get token", 0)
    }

    suspend fun login(email: String, password: String) {
        networkUtil.retry {
            authApi.login(LoginRequest(email, password))
        }.token?.let {
            preferencesDao.authCredentials = AuthCredentials(it, true)
        } ?: throw RequestException("Cannot get token", 0)
    }
}
