package com.sibdever.mindstorm.data.dao.api.comon

import com.sibdever.mindstorm.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.component.KoinComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitCreator(
    private val interceptors: List<Interceptor>
) : KoinComponent {

    fun provideRetrofit(): Retrofit {

        val baseUrl = buildUrl()
        val okHttpClient = createOkHttpClient()

        return createRetrofit(baseUrl, okHttpClient)
    }

    private fun buildUrl() = PREFIX + BuildConfig.URL_BASE + API_VERSION_PREFIX

    private fun createOkHttpClient(): OkHttpClient =
        OkHttpClient().newBuilder()
            .apply {

                interceptors.forEach(::addInterceptor)

                if (BuildConfig.DEBUG) {
                    addInterceptor(
                        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
                    )
                }
            }
            .connectTimeout(CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()

    private fun createRetrofit(
        baseUrl: String,
        okHttpClient: OkHttpClient
    ) = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    companion object {

        private const val PREFIX = "http://"
        private const val API_VERSION_PREFIX = "/v1/"

        private const val CONNECT_TIMEOUT_SECONDS = 30L
        private const val READ_TIMEOUT_SECONDS = 30L
        private const val WRITE_TIMEOUT_SECONDS = 30L
    }
}
