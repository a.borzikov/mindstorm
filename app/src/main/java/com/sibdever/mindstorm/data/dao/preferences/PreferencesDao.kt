package com.sibdever.mindstorm.data.dao.preferences

import android.content.Context
import androidx.core.content.edit
import com.sibdever.mindstorm.domain.model.AuthCredentials
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class PreferencesDao(context: Context) {

    companion object {

        private const val PRES_NAME = "mind_storm_prefs"

        private const val KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN"
        private const val KEY_USER_REGISTERED = "KEY_USER_REGISTERED"
    }

    private val prefs = context.getSharedPreferences(PRES_NAME, Context.MODE_PRIVATE)

    private val _authCredentialsFlow = MutableStateFlow(authCredentials)
    val authCredentialsFlow = _authCredentialsFlow.asStateFlow()

    var authCredentials: AuthCredentials?
        get(): AuthCredentials? {
            val accessTokenValue = accessToken ?: return null

            return AuthCredentials(
                accessToken = accessTokenValue,
                registered = userRegistered
            )
        }
        set(value) {
            accessToken = value?.accessToken
            userRegistered = value?.registered ?: false

            _authCredentialsFlow.value = value
        }

    private var accessToken: String?
        get() = prefs.getString(KEY_ACCESS_TOKEN, null)
        set(value) {
            prefs.edit { putString(KEY_ACCESS_TOKEN, value) }
        }

    private var userRegistered: Boolean
        get() = prefs.getBoolean(KEY_USER_REGISTERED, false)
        set(value) {
            prefs.edit { putBoolean(KEY_USER_REGISTERED, value) }
        }
}
