package com.sibdever.mindstorm.data.di

import com.google.gson.Gson
import com.sibdever.mindstorm.data.dao.api.auth.AuthApi
import com.sibdever.mindstorm.data.dao.api.comon.ApiResponseParser
import com.sibdever.mindstorm.data.dao.api.comon.HeadersInterceptor
import com.sibdever.mindstorm.data.dao.api.comon.NetworkUtil
import com.sibdever.mindstorm.data.dao.api.comon.RetrofitCreator
import com.sibdever.mindstorm.data.dao.preferences.PreferencesDao
import com.sibdever.mindstorm.data.repository.AuthRepository
import okhttp3.Interceptor
import org.koin.dsl.module
import retrofit2.Retrofit

object DataModule {

    val daos = module {
        single { PreferencesDao(get()) }

        // Api
        single { provideAuthApi(get()) }

        // Network
        single { RetrofitCreator(provideInterceptors(get())).provideRetrofit() }
        single { NetworkUtil(get(), get()) }
        single { ApiResponseParser(get()) }
        single { Gson() }
    }

    val repositories = module {
        single { AuthRepository(get(), get(), get()) }
    }

    private fun provideAuthApi(retrofit: Retrofit) = retrofit.create(AuthApi::class.java)

    private fun provideInterceptors(preferencesDao: PreferencesDao): List<Interceptor> =
        listOf(
            HeadersInterceptor(preferencesDao)
        )
}
