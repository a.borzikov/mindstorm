package com.sibdever.mindstorm.data.dao.api.auth.request

data class LoginRequest(
    val email: String,
    val password: String
)
