package com.sibdever.mindstorm.data.dao.api.comon

import com.google.gson.Gson
import com.sibdever.mindstorm.exceptions.RequestException
import com.sibdever.mindstorm.exceptions.ServerFatalException
import com.sibdever.mindstorm.exceptions.UnauthorizedException
import retrofit2.Response
import java.net.HttpURLConnection

class ApiResponseParser(
    private val gson: Gson
) {

    @Throws(Exception::class)
    fun <T> parseResponse(response: Response<T>): T =
        if (response.isSuccessful) {
            response.body() ?: throw RequestException("Response body null", response.code())
        } else {
            val code = response.code()
            if (code == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                throw ServerFatalException(response.message(), code)
            }
            val desc = "Something went wrong..."
            when (code) {
                HttpURLConnection.HTTP_UNAUTHORIZED -> throw UnauthorizedException(desc, code)
                else -> throw RequestException(desc, code)
            }
        }
}
