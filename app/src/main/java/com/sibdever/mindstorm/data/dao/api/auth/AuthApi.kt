package com.sibdever.mindstorm.data.dao.api.auth

import com.sibdever.mindstorm.data.dao.api.auth.request.LoginRequest
import com.sibdever.mindstorm.data.dao.api.auth.request.RegisterRequest
import com.sibdever.mindstorm.data.dao.api.auth.response.LoginResponse
import com.sibdever.mindstorm.data.dao.api.auth.response.RegisterResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {

    companion object {

        const val AUTH_PATH = "user/"
    }

    @POST("${AUTH_PATH}login")
    suspend fun login(
        @Body request: LoginRequest
    ): Response<LoginResponse>

    @POST("${AUTH_PATH}register")
    suspend fun register(
        @Body request: RegisterRequest
    ): Response<RegisterResponse>
}
