package com.sibdever.mindstorm.data.dao.api.comon

import com.sibdever.mindstorm.data.dao.preferences.PreferencesDao
import com.sibdever.mindstorm.exceptions.UnauthorizedException
import org.koin.core.component.KoinComponent
import retrofit2.Response
import java.net.ProtocolException
import java.net.SocketTimeoutException

class NetworkUtil(
    private val apiResponseParser: ApiResponseParser,
    private val preferencesDao: PreferencesDao
) : KoinComponent {

    @Throws(Exception::class)
    suspend fun <T> retry(
        tryCount: Int = DEFAULT_RETRY_COUNT,
        apiCall: suspend () -> Response<T>
    ): T {
        return try {
            val response = apiCall()
            apiResponseParser.parseResponse(response)
        } catch (e: ProtocolException) {
            if (tryCount <= 1) {
                throw e
            }
            retry(tryCount - 1, apiCall)
        } catch (e: UnauthorizedException) {
            if (tryCount < 1) {
                throw e
            }
            preferencesDao.authCredentials = null
            retry(tryCount - 1, apiCall)
        } catch (e: SocketTimeoutException) {
            if (tryCount <= 1) {
                throw e
            }
            retry(tryCount - 1, apiCall)
        }
    }

    companion object {

        private const val DEFAULT_RETRY_COUNT = 3
    }
}
