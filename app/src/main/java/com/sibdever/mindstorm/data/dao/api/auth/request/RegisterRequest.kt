package com.sibdever.mindstorm.data.dao.api.auth.request

data class RegisterRequest(
    val email: String,
    val username: String,
    val password: String
)
