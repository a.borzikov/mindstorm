package com.sibdever.mindstorm.helper

import com.github.terrakok.cicerone.Router
import com.sibdever.mindstorm.data.dao.preferences.PreferencesDao
import com.sibdever.mindstorm.ui.navigations.Screens
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.launch

class AuthCredentialsHelper(
    private val globalRouter: Router,
    private val preferencesDao: PreferencesDao
) {

    private var initialized = false

    val isLoggedIn: Boolean
        get() = preferencesDao.authCredentialsFlow.value
            ?.let { it.accessToken.isNotBlank() && it.registered } ?: false

    fun observeCredentials() {
        if (initialized) return
        initialized = true

        MainScope().launch {
            preferencesDao.authCredentialsFlow
                .drop(1)
                .collectLatest { credentials ->
                    if (credentials == null) globalRouter.newRootScreen(Screens.root())
                }
        }
    }
}
