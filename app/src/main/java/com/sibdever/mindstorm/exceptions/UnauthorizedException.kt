package com.sibdever.mindstorm.exceptions

class UnauthorizedException(
    message: String,
    private val code: Int
) : Exception(message)
