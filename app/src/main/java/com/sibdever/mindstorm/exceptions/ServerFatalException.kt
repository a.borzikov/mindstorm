package com.sibdever.mindstorm.exceptions

open class ServerFatalException(
    message: String,
    private val code: Int
) : Exception(message)
