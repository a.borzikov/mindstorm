package com.sibdever.mindstorm.exceptions

open class RequestException(
    message: String,
    private val code: Int
) : Exception(message)
