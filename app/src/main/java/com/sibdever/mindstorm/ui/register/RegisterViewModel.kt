package com.sibdever.mindstorm.ui.register

import com.github.terrakok.cicerone.Router
import com.sibdever.mindstorm.domain.model.LoadableState
import com.sibdever.mindstorm.domain.usecase.RegisterUseCase
import com.sibdever.mindstorm.ui.common.BaseViewModel
import com.sibdever.mindstorm.ui.navigations.Screens
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class RegisterViewModel(private val router: Router, private val registerUseCase: RegisterUseCase) :
    BaseViewModel() {

    private val _registerState = MutableStateFlow<LoadableState<Boolean>>(LoadableState.Initial)
    val registerState = _registerState.asStateFlow()

    override fun initialize() {
        // no op
    }

    fun onLoginLinkClick() {
        router.replaceScreen(Screens.login())
    }

    fun onRegisterClick(email: String?, username: String?, password: String?) {
        if (!email.isNullOrBlank() && !username.isNullOrBlank() && !password.isNullOrBlank()) {
            safeLaunch(
                block = {
                    _registerState.value = LoadableState.Loading
                    registerUseCase(email, username, password)
                    _registerState.value = LoadableState.Success(true)
                    router.newRootScreen(Screens.main())
                },
                onError = {
                    _registerState.value = LoadableState.Error(it)
                }
            )
        }
    }
}
