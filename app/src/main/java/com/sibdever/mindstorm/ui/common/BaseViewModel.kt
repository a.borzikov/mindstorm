package com.sibdever.mindstorm.ui.common

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sibdever.mindstorm.ui.MindStormApp
import kotlinx.coroutines.launch

abstract class BaseViewModel : ViewModel() {

    private var inited = false

    fun provideInitialData() {

        if (inited) {
            return
        }
        inited = true

        initialize()
    }

    protected abstract fun initialize()

    override fun onCleared() {
        super.onCleared()
    }

    protected open fun safeLaunch(
        block: suspend () -> Unit,
        onError: (Throwable) -> Unit = {}
    ) = viewModelScope.launch {
        try {
            block()
        } catch (e: Throwable) {
            Log.e(MindStormApp.TAG, "safeLaunch", e)
            onError(e)
        }
    }
}
