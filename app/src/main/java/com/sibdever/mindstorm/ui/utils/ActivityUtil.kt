package com.sibdever.mindstorm.ui.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

fun Activity.hideKeyboard() {
    val manager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    manager.hideSoftInputFromWindow(window.decorView.windowToken, 0)
    manager.hideSoftInputFromWindow(window.decorView.applicationWindowToken, 0)
}

fun Activity.hideKeyboardAndClearFocus(view: View? = currentFocus) {
    hideKeyboard()
    clearFocus(view)
}

fun Activity.clearFocus(view: View? = currentFocus) {
    view?.clearFocus()
}
