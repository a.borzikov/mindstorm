package com.sibdever.mindstorm.ui.login

import com.github.terrakok.cicerone.Router
import com.sibdever.mindstorm.domain.model.LoadableState
import com.sibdever.mindstorm.domain.usecase.LoginUseCase
import com.sibdever.mindstorm.ui.common.BaseViewModel
import com.sibdever.mindstorm.ui.navigations.Screens
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class LoginViewModel(private val router: Router, private val loginUseCase: LoginUseCase) :
    BaseViewModel() {

    private val _loginState = MutableStateFlow<LoadableState<Boolean>>(LoadableState.Initial)
    val loginState = _loginState.asStateFlow()

    override fun initialize() {
        // no op
    }

    fun onRegisterLinkClick() {
        router.replaceScreen(Screens.register())
    }

    fun onLoginClick(email: String?, password: String?) {
        if (!email.isNullOrBlank() && !password.isNullOrBlank()) {
            safeLaunch(
                block = {
                    _loginState.value = LoadableState.Loading
                    loginUseCase(email, password)
                    _loginState.value = LoadableState.Success(true)
                    router.newRootScreen(Screens.main())
                },
                onError = {
                    _loginState.value = LoadableState.Error(it)
                }
            )
        }
    }
}
