package com.sibdever.mindstorm.ui

import android.app.Application
import com.sibdever.mindstorm.BuildConfig
import com.sibdever.mindstorm.data.di.DataModule
import com.sibdever.mindstorm.domain.di.DomainModule
import com.sibdever.mindstorm.ui.di.UiModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MindStormApp : Application() {

    companion object {

        const val TAG = "MIND_STORM"

        internal lateinit var INSTANCE: MindStormApp
            private set
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@MindStormApp)
            modules(
                listOf(
                    DataModule.daos,
                    DataModule.repositories,
                    DomainModule.useCases,
                    DomainModule.helpers,
                    UiModule.navigation,
                    UiModule.viewModels
                )
            )
        }
        INSTANCE = this
    }
}
