package com.sibdever.mindstorm.ui.main

import android.view.LayoutInflater
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.androidx.AppNavigator
import com.sibdever.mindstorm.R
import com.sibdever.mindstorm.databinding.FragmentMainBinding
import com.sibdever.mindstorm.ui.common.BaseFragment
import com.sibdever.mindstorm.ui.di.UiModule
import com.sibdever.mindstorm.ui.navigations.Screens
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.qualifier.named

class MainFragment : BaseFragment<FragmentMainBinding>() {

    override fun inflateBinding(inflater: LayoutInflater) = FragmentMainBinding.inflate(inflater)

    override val viewModel by viewModel<MainViewModel>()

    private val navigatorHolder by inject<NavigatorHolder>(named(UiModule.MAIN_SCOPE))
    private val navigator by lazy {
        AppNavigator(
            requireActivity(),
            R.id.fragment_container,
            childFragmentManager
        )
    }

    override fun setupUi() {
        setupNavigation()
    }

    private fun setupNavigation() {
        binding.bottomNavigation.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.menu_item_home -> viewModel.replaceScreen(Screens.invites())
                R.id.menu_item_statistic -> viewModel.replaceScreen(Screens.statistic())
            }
            return@setOnItemSelectedListener true
        }
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }
}
