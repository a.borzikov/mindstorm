package com.sibdever.mindstorm.ui.welcome

import android.view.LayoutInflater
import com.sibdever.mindstorm.databinding.FragmentWelcomeBinding
import com.sibdever.mindstorm.ui.common.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class WelcomeFragment : BaseFragment<FragmentWelcomeBinding>() {

    override val viewModel by viewModel<WelcomeViewModel>()

    override fun inflateBinding(inflater: LayoutInflater): FragmentWelcomeBinding =
        FragmentWelcomeBinding.inflate(inflater)
}
