package com.sibdever.mindstorm.ui.invites

data class SessionItem(
    val id: Int,
    val name: String,
    val description: String,
    val status: String,
    val daysLeft: Int,
    val hoursLeft: Int,
    val minutesLeft: Int
)
