package com.sibdever.mindstorm.ui.welcome

import androidx.lifecycle.viewModelScope
import com.github.terrakok.cicerone.Router
import com.sibdever.mindstorm.ui.common.BaseViewModel
import com.sibdever.mindstorm.ui.navigations.Screens
import kotlinx.coroutines.launch
import java.util.Timer
import kotlin.concurrent.schedule

private const val SHOWING_SPLASH_TIME = 2000L
private const val SHOWING_SPLASH_TIMER_NAME = "ShowingSplash"

class WelcomeViewModel(
    private val router: Router
) : BaseViewModel() {

    override fun initialize() {
        viewModelScope.launch {
            setupTimeout()
        }
    }

    private fun setupTimeout() {
        Timer(SHOWING_SPLASH_TIMER_NAME, false).schedule(SHOWING_SPLASH_TIME) {
            navigateNext()
        }
    }

    private fun navigateNext() {
        router.newRootScreen(Screens.root())
    }
}
