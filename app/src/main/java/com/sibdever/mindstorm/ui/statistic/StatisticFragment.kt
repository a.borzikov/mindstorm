package com.sibdever.mindstorm.ui.statistic

import android.view.LayoutInflater
import com.sibdever.mindstorm.databinding.FragmentStatisticBinding
import com.sibdever.mindstorm.ui.common.BaseFragment

class StatisticFragment : BaseFragment<FragmentStatisticBinding>() {

    override fun inflateBinding(inflater: LayoutInflater) =
        FragmentStatisticBinding.inflate(inflater)


}
