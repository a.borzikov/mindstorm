package com.sibdever.mindstorm.ui.register

import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.sibdever.mindstorm.databinding.FragmentRegisterBinding
import com.sibdever.mindstorm.domain.model.LoadableState
import com.sibdever.mindstorm.domain.model.isLoading
import com.sibdever.mindstorm.ui.common.BaseFragment
import com.sibdever.mindstorm.ui.utils.hideKeyboardAndClearFocus
import com.sibdever.mindstorm.ui.utils.makeLinks
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterFragment : BaseFragment<FragmentRegisterBinding>() {

    override fun inflateBinding(inflater: LayoutInflater) =
        FragmentRegisterBinding.inflate(inflater)

    override val viewModel by viewModel<RegisterViewModel>()

    override fun setupUi() {
        setupLink()
        setupState()
        setupListeners()
    }

    private fun setupLink() {
        binding.alreadyRegistered.makeLinks(
            "Login here!" to View.OnClickListener {
                viewModel.onLoginLinkClick()
            }
        )
    }

    private fun setupState() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.registerState.collectLatest {
                binding.progress.isVisible = it.isLoading()
                binding.contentContainer.isVisible = !binding.progress.isVisible
                if (it is LoadableState.Error) showErrorMessage()
            }
        }
    }

    private fun setupListeners() {
        binding.registerButton.setOnClickListener {
            requireActivity().hideKeyboardAndClearFocus()
            val password = binding.passwordEditText.text.toString()
            val confirmPassword = binding.confirmPasswordEditText.text.toString()
            if (password.isBlank() || password != confirmPassword) {
                Toast
                    .makeText(requireContext(), "Passwords don't match", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            viewModel.onRegisterClick(
                email = binding.emailEditText.text.toString(),
                username = binding.nameEditText.text.toString(),
                password = binding.passwordEditText.text.toString()
            )
        }
    }

    private fun showErrorMessage() {
        Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT)
            .show()
    }
}
