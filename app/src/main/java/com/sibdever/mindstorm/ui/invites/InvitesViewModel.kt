package com.sibdever.mindstorm.ui.invites

import com.sibdever.mindstorm.domain.model.LoadableState
import com.sibdever.mindstorm.ui.common.BaseViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class InvitesViewModel : BaseViewModel() {

    private val _sessionsState =
        MutableStateFlow<LoadableState<List<SessionItem>>>(LoadableState.Initial)
    val sessionsState = _sessionsState.asStateFlow()

    override fun initialize() {
        loadData()
    }

    private fun loadData() {
        safeLaunch(
            block = {
                _sessionsState.value = LoadableState.Loading
                delay(2_000)
                // TODO: Get it from backend
                _sessionsState.value = LoadableState.Success(
                    List(12) {
                        SessionItem(
                            it,
                            "Brainstorm $it",
                            "Description $it",
                            "Brainstorming",
                            it / 10,
                            it % 10 + 1,
                            it % 3 + 1
                        )
                    }
                )
            },
            onError = {
                _sessionsState.value = LoadableState.Error(it)
            }

        )
    }
}
