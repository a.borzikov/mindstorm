package com.sibdever.mindstorm.ui.root

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.androidx.AppNavigator
import com.sibdever.mindstorm.R
import com.sibdever.mindstorm.databinding.ActivityRootBinding
import com.sibdever.mindstorm.ui.di.UiModule.GLOBAL_SCOPE
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.qualifier.named

class RootActivity : AppCompatActivity() {

    lateinit var binding: ActivityRootBinding
    private val navigatorHolder by inject<NavigatorHolder>(named(GLOBAL_SCOPE))
    private val navigator = AppNavigator(this, R.id.root_fragment)

    private val viewModel by viewModel<RootViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRootBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.provideInitialData()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }
}
