package com.sibdever.mindstorm.ui.login

import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.sibdever.mindstorm.databinding.FragmentLoginBinding
import com.sibdever.mindstorm.domain.model.LoadableState
import com.sibdever.mindstorm.domain.model.isLoading
import com.sibdever.mindstorm.ui.common.BaseFragment
import com.sibdever.mindstorm.ui.utils.hideKeyboardAndClearFocus
import com.sibdever.mindstorm.ui.utils.makeLinks
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    override fun inflateBinding(inflater: LayoutInflater) = FragmentLoginBinding.inflate(inflater)

    override val viewModel by viewModel<LoginViewModel>()

    override fun setupUi() {
        setupLink()
        setupState()
        setupListeners()
    }

    private fun setupLink() {
        binding.notRegistered.makeLinks(
            "Register here!" to View.OnClickListener {
                viewModel.onRegisterLinkClick()
            }
        )
    }

    private fun setupState() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.loginState.collectLatest {
                binding.progress.isVisible = it.isLoading()
                binding.contentContainer.isVisible = !binding.progress.isVisible
                if (it is LoadableState.Error) showErrorMessage()
            }
        }
    }

    private fun setupListeners() {
        binding.loginButton.setOnClickListener {
            requireActivity().hideKeyboardAndClearFocus()
            viewModel.onLoginClick(
                email = binding.emailEditText.text.toString(),
                password = binding.passwordEditText.text.toString()
            )
        }
    }

    private fun showErrorMessage() {
        Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT)
            .show()
    }
}
