package com.sibdever.mindstorm.ui.main

import com.github.terrakok.cicerone.Router
import com.github.terrakok.cicerone.Screen
import com.sibdever.mindstorm.ui.common.BaseViewModel

class MainViewModel(
    private val router: Router
) : BaseViewModel() {

    override fun initialize() {
        // no op
    }

    fun replaceScreen(screen: Screen) {
        router.replaceScreen(screen)
    }
}
