package com.sibdever.mindstorm.ui.navigations

import android.content.Intent
import com.github.terrakok.cicerone.androidx.ActivityScreen
import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.sibdever.mindstorm.ui.invites.InvitesFragment
import com.sibdever.mindstorm.ui.login.LoginFragment
import com.sibdever.mindstorm.ui.main.MainFragment
import com.sibdever.mindstorm.ui.register.RegisterFragment
import com.sibdever.mindstorm.ui.root.RootActivity
import com.sibdever.mindstorm.ui.statistic.StatisticFragment

object Screens {

    fun root() = ActivityScreen { Intent(it, RootActivity::class.java) }

    fun login() = FragmentScreen { LoginFragment() }

    fun register() = FragmentScreen { RegisterFragment() }

    fun main() = FragmentScreen { MainFragment() }

    fun invites() = FragmentScreen { InvitesFragment() }

    fun statistic() = FragmentScreen { StatisticFragment() }
}
