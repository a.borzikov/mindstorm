package com.sibdever.mindstorm.ui.invites

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import com.sibdever.mindstorm.databinding.LayoutSessionItemBinding

class SessionsAdapter : AsyncListDifferDelegationAdapter<SessionItem>(
    DiffUtilCallback(),
    sessionsAdapterDelegate()

) {

    class DiffUtilCallback : DiffUtil.ItemCallback<SessionItem>() {

        override fun areItemsTheSame(oldItem: SessionItem, newItem: SessionItem) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: SessionItem, newItem: SessionItem) =
            oldItem == newItem
    }
}

fun sessionsAdapterDelegate() =
    adapterDelegateViewBinding<SessionItem, SessionItem, LayoutSessionItemBinding>(
        { layoutInflater, parent ->
            LayoutSessionItemBinding.inflate(
                layoutInflater,
                parent,
                false
            )
        }
    ) {
        bind {
            with(binding) {
                title.text = item.name
                description.text = item.description
                status.text = item.status
                if (item.daysLeft == 0) {
                    if (item.hoursLeft == 0) {
                        timeText.text = "${item.minutesLeft}m"
                    } else {
                        timeText.text = "${item.hoursLeft}Hr:${item.minutesLeft}m"
                    }
                } else {
                    timeText.text = "${item.daysLeft}D ${item.hoursLeft}Hr:${item.minutesLeft}m"
                }
            }
        }
    }
