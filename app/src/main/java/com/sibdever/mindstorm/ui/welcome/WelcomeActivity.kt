package com.sibdever.mindstorm.ui.welcome

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.androidx.AppNavigator
import com.sibdever.mindstorm.R
import com.sibdever.mindstorm.databinding.ActivityWelcomeBinding
import com.sibdever.mindstorm.ui.di.UiModule.GLOBAL_SCOPE
import org.koin.android.ext.android.inject
import org.koin.core.qualifier.named

class WelcomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWelcomeBinding
    private val navigatorHolder by inject<NavigatorHolder>(named(GLOBAL_SCOPE))
    private val navigator = AppNavigator(this, R.id.welcome_fragment_container)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }
}
