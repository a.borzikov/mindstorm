package com.sibdever.mindstorm.ui.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<Binding : ViewBinding> : Fragment() {

    protected open val viewModel: BaseViewModel? = null
    private var _binding: Binding? = null
    protected val binding: Binding get() = _binding!!

    protected abstract fun inflateBinding(inflater: LayoutInflater): Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflateBinding(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel?.provideInitialData()
        setupUi()
    }

    protected open fun setupUi() {}

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
