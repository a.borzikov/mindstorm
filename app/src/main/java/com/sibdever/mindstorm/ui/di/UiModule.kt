package com.sibdever.mindstorm.ui.di

import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Router
import com.sibdever.mindstorm.ui.invites.InvitesViewModel
import com.sibdever.mindstorm.ui.login.LoginViewModel
import com.sibdever.mindstorm.ui.main.MainViewModel
import com.sibdever.mindstorm.ui.register.RegisterViewModel
import com.sibdever.mindstorm.ui.root.RootViewModel
import com.sibdever.mindstorm.ui.welcome.WelcomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

object UiModule {

    const val MAIN_SCOPE = "MAIN"
    const val GLOBAL_SCOPE = "GLOBAL"
    const val BRAINSTORM_SCOPE = "BRAINSTORM"
    const val STATISTIC_SCOPE = "STATISTIC"

    val viewModels = module {
        viewModel { WelcomeViewModel(get(named(GLOBAL_SCOPE))) }
        viewModel { RootViewModel(get(named(GLOBAL_SCOPE)), get()) }

        viewModel { LoginViewModel(get(named(GLOBAL_SCOPE)), get()) }
        viewModel { RegisterViewModel(get(named(GLOBAL_SCOPE)), get()) }

        viewModel { MainViewModel(get(named(MAIN_SCOPE))) }

        viewModel { InvitesViewModel() }
    }

    val navigation = module {
        // Global
        single(named(GLOBAL_SCOPE)) { Cicerone.create(Router()) }
        single(named(GLOBAL_SCOPE)) { get<Cicerone<Router>>(named(GLOBAL_SCOPE)).router }
        single(named(GLOBAL_SCOPE)) {
            get<Cicerone<Router>>(named(GLOBAL_SCOPE)).getNavigatorHolder()
        }
        // Main
        single(named(MAIN_SCOPE)) { Cicerone.create(Router()) }
        single(named(MAIN_SCOPE)) { get<Cicerone<Router>>(named(MAIN_SCOPE)).router }
        single(named(MAIN_SCOPE)) {
            get<Cicerone<Router>>(named(MAIN_SCOPE)).getNavigatorHolder()
        }
        // Brainstorm
        single(named(BRAINSTORM_SCOPE)) { Cicerone.create(Router()) }
        single(named(BRAINSTORM_SCOPE)) { get<Cicerone<Router>>(named(BRAINSTORM_SCOPE)).router }
        single(named(BRAINSTORM_SCOPE)) {
            get<Cicerone<Router>>(named(BRAINSTORM_SCOPE)).getNavigatorHolder()
        }
        // Statistic
        single(named(STATISTIC_SCOPE)) { Cicerone.create(Router()) }
        single(named(STATISTIC_SCOPE)) { get<Cicerone<Router>>(named(STATISTIC_SCOPE)).router }
        single(named(STATISTIC_SCOPE)) {
            get<Cicerone<Router>>(named(STATISTIC_SCOPE)).getNavigatorHolder()
        }
    }
}
