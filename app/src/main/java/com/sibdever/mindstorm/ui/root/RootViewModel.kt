package com.sibdever.mindstorm.ui.root

import com.github.terrakok.cicerone.Router
import com.sibdever.mindstorm.helper.AuthCredentialsHelper
import com.sibdever.mindstorm.ui.common.BaseViewModel
import com.sibdever.mindstorm.ui.navigations.Screens

class RootViewModel(
    private val router: Router,
    private val authCredentialsHelper: AuthCredentialsHelper
) : BaseViewModel() {

    override fun initialize() {
        authCredentialsHelper.observeCredentials()
        setupInitialScreen()
    }

    private fun setupInitialScreen() {
        router.replaceScreen(
            if (authCredentialsHelper.isLoggedIn) {
                Screens.main()
            } else {
                Screens.login()
            }
        )
    }
}
