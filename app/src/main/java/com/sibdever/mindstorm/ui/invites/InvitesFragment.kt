package com.sibdever.mindstorm.ui.invites

import android.view.LayoutInflater
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.sibdever.mindstorm.R
import com.sibdever.mindstorm.databinding.FragmentInvitesBinding
import com.sibdever.mindstorm.domain.model.forceData
import com.sibdever.mindstorm.domain.model.isLoading
import com.sibdever.mindstorm.ui.common.BaseFragment
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel

class InvitesFragment : BaseFragment<FragmentInvitesBinding>() {

    override fun inflateBinding(inflater: LayoutInflater) = FragmentInvitesBinding.inflate(inflater)

    override val viewModel by viewModel<InvitesViewModel>()

    override fun setupUi() {
        setupSessions()
    }

    private fun setupSessions() {
        with(binding.recycler) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = SessionsAdapter()
            addItemDecoration(
                DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL).apply {
                    setDrawable(
                        AppCompatResources.getDrawable(
                            requireContext(),
                            R.drawable.bg_vertical_space_27
                        )!!
                    )
                }
            )
        }
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.sessionsState.collectLatest {
                binding.progress.isVisible = it.isLoading()
                binding.recycler.isVisible = !binding.progress.isVisible
                it.forceData()?.let { items ->
                    (binding.recycler.adapter as SessionsAdapter).items = items
                }
            }
        }
    }
}
